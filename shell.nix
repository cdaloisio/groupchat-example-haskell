{ pkgs ? import <nixpkgs> {} }:

with pkgs;

pkgs.stdenv.mkDerivation {
  name = "groupChatEnv";
  buildInputs = [
    terraform_0_12
    terraform-providers.aws
    awscli
  ];
}

