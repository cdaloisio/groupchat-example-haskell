module Main where

import           System.IO                      ( IOMode(..)
                                                , hClose
                                                )
import qualified Network.Socket                as Socket
import           Text.Printf                    ( printf )
import           Control.Concurrent             ( forkFinally )
import           Control.Monad

import           Chat                           ( newServer
                                                , newSession
                                                , talk
                                                )

main :: IO ()
main = Socket.withSocketsDo $ do
  server  <- newServer
  session <- newSession 1
  let hints = Socket.defaultHints
        { Socket.addrFlags      = [Socket.AI_NUMERICHOST, Socket.AI_NUMERICSERV]
        , Socket.addrSocketType = Socket.Stream
        , Socket.addrFamily     = Socket.AF_INET
        }
  addr : _ <- Socket.getAddrInfo (Just hints)
                                 (Just "0.0.0.0")
                                 (Just $ show port)
  socket <- Socket.socket (Socket.addrFamily addr)
                          (Socket.addrSocketType addr)
                          (Socket.addrProtocol addr)
  Socket.bind socket (Socket.addrAddress addr)
  Socket.listen socket port
  printf "Listening on port %d\n" port
  forever $ do
    (sock', sockAddr) <- Socket.accept socket
    handle            <- Socket.socketToHandle sock' ReadWriteMode
    printf "Accepted connection from %s: %s\n" (show sockAddr) (show port)
    forkFinally (talk handle session) (\_ -> hClose handle)

port :: Int
port = 8080
