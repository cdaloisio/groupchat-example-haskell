{ mkDerivation, async, base, containers, network, stdenv, stm, text
, time
}:
mkDerivation {
  pname = "groupchat";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    async base containers network stm text time
  ];
  executableHaskellDepends = [ base ];
  license = stdenv.lib.licenses.mit;
}
