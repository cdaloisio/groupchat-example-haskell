#! /usr/bin/env bash

SERVERIP="13.210.111.157"

SERVER="root@${SERVERIP}"
BUILDPATH=$(nix-build --no-out-link server.nix | tail -1)
SYSTEM_PROFILE="/nix/var/nix/profiles/system"
nix-copy-closure --to --use-substitutes $SERVER $BUILDPATH
ssh $SERVER "sudo nix-env -p '${SYSTEM_PROFILE}' --set '${BUILDPATH}'"
ssh $SERVER "sudo ${SYSTEM_PROFILE}/bin/switch-to-configuration switch"

APP_BUILDPATH=$(nix-build --no-out-link release.nix | tail -1)
USER_SERVER="deployer@${SERVERIP}"
nix-copy-closure --to --use-substitutes $USER_SERVER $APP_BUILDPATH
ssh $USER_SERVER "${APP_BUILDPATH}/bin/groupchat"
