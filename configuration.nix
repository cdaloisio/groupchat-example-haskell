{ pkgs, ... }: let
  groupchat = import ./release.nix;
in
{
  imports = [ <nixpkgs/nixos/modules/virtualisation/amazon-image.nix> ];
  ec2.hvm = true;
  networking.hostName = "groupchat-demo";
  security.sudo.wheelNeedsPassword = false;
  services.openssh.passwordAuthentication = false;
  nix.trustedUsers = ["@wheel"];
   networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 22 80 8080 ];
  };

  services.nginx = {
    enable = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
  };

  environment.systemPackages = [ pkgs.vim pkgs.htop pkgs.telnet ];

  users.users.deployer= {
    isNormalUser = true;
    extraGroups = [ "wheel" ];

    openssh.authorizedKeys.keys = [
      "${builtins.readFile ./keys/deployer.pub}"
    ];
  };

  systemd.services.groupchat =
    {
      description = "groupchat Webserver";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = {
        ExecStart = "${groupchat}/bin/groupchat";
        Restart="on-failure";
        RestartSec=5;
      };
    };
}
