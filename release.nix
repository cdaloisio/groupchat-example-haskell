let
  pkgs = import <nixpkgs> { };
  gitignore = pkgs.nix-gitignore.gitignoreSource [] ./.;
in
  pkgs.haskellPackages.callCabal2nix "groupchat" gitignore {}
