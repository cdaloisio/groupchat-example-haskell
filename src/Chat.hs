{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}

module Chat where

import           System.IO                      ( Handle
                                                , BufferMode(..)
                                                , IOMode(..)
                                                , hPutStrLn
                                                , hGetLine
                                                , hSetNewlineMode
                                                , hSetBuffering
                                                , universalNewlineMode
                                                )
import           Control.Concurrent.Async       ( race )
import           Control.Concurrent.STM         ( TChan
                                                , TVar
                                                , atomically
                                                , newTVarIO
                                                , newTChan
                                                , newTVar
                                                , readTVar
                                                , readTChan
                                                , writeTChan
                                                , writeTVar
                                                , modifyTVar'
                                                )
import           Control.Exception              ( mask
                                                , finally
                                                )
import           Text.Printf                    ( hPrintf )
import           Control.Monad

import           Data.Map                       ( Map )
import qualified Data.Map                      as Map
import qualified Data.Time                     as Time
import qualified Data.Text                     as T

import qualified User                          as U

data Server = Server
  { _sessions :: TVar (Map SessionId Session)
  }

newServer = do
  sessions <- newTVarIO Map.empty
  return Server { _sessions = sessions }

type SessionId = Int

data Session = Session
  { _sessionId :: SessionId
  , _clients :: TVar (Map U.User Client)
  , _posts :: TVar [Post]
  }

newSession sessionId = do
  clients <- newTVarIO Map.empty
  posts   <- newTVarIO []
  return Session { _sessionId = sessionId, _clients = clients, _posts = posts }

data Client = Client
  { _clientUser :: U.User
  , _clientHandle :: Handle
  , _clientKicked   :: TVar (Maybe Post)
  , _clientSendChan :: TChan Message
  }

newClient user handle = do
  sendChannel <- newTChan
  kicked      <- newTVar Nothing
  return Client { _clientUser     = user
                , _clientHandle   = handle
                , _clientKicked   = kicked
                , _clientSendChan = sendChannel
                }

data Message
  = Notice T.Text
  | Tell U.User Post
  | Broadcast U.User T.Text
  | Command T.Text

data Post
  = FlaggedPost PostAttributes
  | StandardPost PostAttributes

data PostAttributes = PostAttributes
  { _userId :: U.UserId
  , _postId :: Int
  , _body :: T.Text
  , _to :: U.User
  , _from :: U.User
  , _timestamp :: Time.UTCTime
  }

mkPost :: (PostAttributes -> Post) -> U.User -> T.Text -> U.User -> Post
mkPost postConstructor to body from = postConstructor postAttributes
 where
  postAttributes = PostAttributes
    { _userId    = U.userId from
    , _postId    = 1
    , _body      = body
    , _to        = to
    , _from      = from
    , _timestamp = Time.UTCTime (Time.ModifiedJulianDay 58423)
                                (Time.secondsToDiffTime 0)
    }


-- Functions

broadcast Session {..} msg = do
  clientMap <- readTVar _clients
  mapM_ (`sendMessage` msg) (Map.elems clientMap)

sendMessage Client {..} = writeTChan _clientSendChan

sendToUser session@Session {..} user msg = do
  clientMap <- readTVar _clients
  case Map.lookup user clientMap of
    Nothing     -> return False
    Just client -> sendMessage client msg >> return True

tell session@Session {..} Client {..} who msg = do
  ok <- atomically $ sendToUser session who (Tell _clientUser msg)
  if ok
    then return ()
    else hPutStrLn _clientHandle
                   (T.unpack $ U.username who <> " is not connected")

kick session@Session {..} who by = do
  clientMap <- readTVar _clients
  case Map.lookup who clientMap of
    Nothing -> void
      $ sendToUser session by (Notice $ U.username who <> " is not connected")
    Just kickedUser -> do
      writeTVar (_clientKicked kickedUser)
        $ Just (mkPost StandardPost who "was kicked" by)
      void $ sendToUser session by (Notice $ "you kicked " <> U.username who)

-- -----------------------------------------------------------------------------
-- The main server

talk :: Handle -> Session -> IO ()
talk handle session@Session {..} = do
  hSetNewlineMode handle universalNewlineMode
      -- Swallow carriage returns sent by telnet clients
  hSetBuffering handle LineBuffering
  readName
 where
-- <<readName
  readName = do
    hPutStrLn handle "What is your name?"
    name <- hGetLine handle
    if null name
      then readName
      else mask $ \restore -> do   -- <1>
        ok <- checkAddClient session (U.findOrCreateUser $ T.pack name) handle
        case ok of
          Nothing -> restore $ do  -- <2>
            hPrintf handle "The name %s is in use, please choose another\n" name
            readName
          Just client ->
            restore (runClient session client) `finally` removeClient
              session
              (U.findOrCreateUser $ T.pack name)
-- >>

-- <<checkAddClient
checkAddClient :: Session -> U.User -> Handle -> IO (Maybe Client)
checkAddClient session@Session {..} user handle = atomically $ do
  clientmap <- readTVar _clients
  if Map.member user clientmap
    then return Nothing
    else do
      client <- newClient user handle
      writeTVar _clients $ Map.insert user client clientmap
      broadcast session $ Notice (U.username user <> " has connected")
      return (Just client)
-- >>

-- <<removeClient
removeClient :: Session -> U.User -> IO ()
removeClient session@Session {..} user = atomically $ do
  modifyTVar' _clients $ Map.delete user
  broadcast session $ Notice (U.username user <> " has disconnected")
-- >>

-- <<runClient
runClient :: Session -> Client -> IO ()
runClient session@Session {..} client@Client {..} = do
  race session' receive
  return ()
 where
  receive = forever $ do
    msg <- hGetLine _clientHandle
    atomically $ sendMessage client (Command (T.pack msg))

  session' = join $ atomically $ do
    k <- readTVar _clientKicked
    case k of
      Just reason ->
        return $ hPutStrLn _clientHandle $ "You have been kicked: " <> T.unpack
          (postBody reason)
      Nothing -> do
        msg <- readTChan _clientSendChan
        return $ do
          continue <- handleMessage session client msg
          when continue session'
-- >>

postBody post = case post of
  StandardPost PostAttributes {..} -> _body
  FlaggedPost  PostAttributes {..} -> _body


-- <<handleMessage
handleMessage :: Session -> Client -> Message -> IO Bool
handleMessage session client@Client {..} message = case message of
  Notice msg -> output $ "*** " <> T.unpack msg
  Tell user msg ->
    output $ "*" <> T.unpack (U.username user) <> "*: " <> T.unpack
      (postBody msg)
  Broadcast user msg ->
    output $ "<" <> T.unpack (U.username user) <> ">: " <> T.unpack msg
  Command msg -> case words (T.unpack msg) of
    ["/kick", who] -> do
      atomically $ kick session (U.findOrCreateUser (T.pack who)) _clientUser
      return True
    "/tell" : who : what -> do
      tell
        session
        client
        (U.findOrCreateUser (T.pack who))
        (mkPost StandardPost
                (U.findOrCreateUser (T.pack who))
                (T.pack $ unwords what)
                _clientUser
        )
      return True
    [         "/quit"] -> return False
    ('/' : _) :      _ -> do
      hPutStrLn _clientHandle $ "Unrecognised command: " <> T.unpack msg
      return True
    _ -> do
      atomically $ broadcast session $ Broadcast _clientUser msg
      return True
 where
  output s = do
    hPutStrLn _clientHandle s
    return True
-- >>
