{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}

module User where

import qualified Data.Text                     as T

data User
  = Moderator UserId UserProfile
  | Participant UserId UserAnonymity
  | Guest UserId UserAnonymity
  deriving (Eq, Ord)

data UserProfile = UserProfile
  { _username :: T.Text
  , _avatar :: T.Text
  } deriving (Eq, Ord)

data UserAnonymity
  = Anonymous
  | Known UserProfile
  deriving (Eq, Ord)

newtype UserId = UserId { unUserId :: Int }
  deriving (Show, Eq, Ord)

findOrCreateUser :: T.Text -> User
findOrCreateUser name = Participant
  (UserId 1)
  (Known $ UserProfile { _username = name, _avatar = "" })

userId :: User -> UserId
userId (Moderator   userId' _) = userId'
userId (Participant userId' _) = userId'
userId (Guest       userId' _) = userId'

username :: User -> T.Text
username user =
  let getProfile anonymity userId' = case anonymity of
        Anonymous     -> T.pack $ "User " <> show userId'
        Known profile -> _username profile
  in  case user of
        Moderator   _userid profile   -> _username profile
        Participant userId' anonymity -> getProfile anonymity userId'
        Guest       userId' anonymity -> getProfile anonymity userId'



