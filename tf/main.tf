provider "aws" {
  region = "ap-southeast-2"
  profile = "ccplayground"
}

data "local_file" "deployer_public_key" {
  filename = "../keys/deployer.pub"
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name   = "deployer"
  public_key = data.local_file.deployer_public_key.content
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name        = "example"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "ssh-tcp", "all-icmp"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  egress_rules        = ["all-all"]
}

resource "aws_eip" "this" {
  vpc      = true
  instance = module.ec2_cluster.id[0]
}

module "ec2_cluster" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "groupchat-cluster"
  instance_count         = 1

  ami                    = "ami-054c73a7f8d773ea9"
  instance_type          = "t2.micro"
  key_name               = "deployer"
  monitoring             = true
  subnet_id              = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids = [module.security_group.this_security_group_id]
  associate_public_ip_address = true

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 10
    },
  ]

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

output "public_ip" {
  description = "Public IP address of the instance"
  value = module.ec2_cluster.public_ip
}
